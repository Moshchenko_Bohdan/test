package com.webdiary.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_records")
public class UserRecord {
    @Id
    @Column
    @GeneratedValue
    private Long id;
    @Column(name = "record")
    private String record;
    @Column(name = "date")
    private Date date;
    @Column(name = "userId")
    private Long userId;

    public UserRecord() {

    }

    public UserRecord(String record, Date date, Long userId) {
        this.record = record;
        this.date = date;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}