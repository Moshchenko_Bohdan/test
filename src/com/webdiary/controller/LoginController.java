package com.webdiary.controller;

import com.webdiary.domain.User;
import com.webdiary.domain.UserRecord;
import com.webdiary.services.UserRecordService;
import com.webdiary.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class LoginController {
    @Autowired
    UserService userService;

    @Autowired
    UserRecordService userRecordService;

    @RequestMapping(value = {"/", "/welcome"})
    public ModelAndView welcome(HttpServletRequest request,
                                @RequestParam(value = "username", required = false) String username,
                                @RequestParam(value = "password", required = false) String password) {
        ModelAndView model;
        if (request.getSession().getAttribute("userId") != null) {
            model = new ModelAndView("userRecords");
            Long userId = (Long) request.getSession().getAttribute("userId");
            model.addObject("username", username);
            Integer pageNumber = 0;
            List<UserRecord> userRecords = userRecordService.pagination(userId, pageNumber);
            model.addObject("userRecords", userRecords);
            Long userRecordsCount = userRecordService.userRecordsRowCount(userId);
            Long pageCount;
            if (userRecordsCount % 10 > 0) {
                pageCount = userRecordsCount / 10 + 1;
            } else {
                pageCount = userRecordsCount / 10;
            }
            model.addObject("pageCount", pageCount);
        } else {
            model = new ModelAndView("welcome");
        }
        return model;
    }

    @RequestMapping(value = "register.action")
    public ModelAndView registerUser(HttpServletRequest request,
                                     @RequestParam(value = "username", required = true) String username,
                                     @RequestParam(value = "password", required = true) String password) {
        User user = new User(username, password);
        boolean status = userService.saveUser(user);
        ModelAndView model = new ModelAndView("registerUser");
        model.addObject("name", username);
        model.addObject("status", status);
        request.getSession().setAttribute("userId", userService.getUserByUsername(username).getId());
        return model;
    }

    @RequestMapping(value = "loginUser.action")
    public ModelAndView loginUser(HttpServletRequest request,
                                  @RequestParam(value = "username", required = true) String username,
                                  @RequestParam(value = "password", required = true) String password) {
        ModelAndView model = new ModelAndView("error");
        if (userService.userAuthentication(username, password)) {
            model = new ModelAndView("userRecords");
            Long userId = userService.getUserByUsername(username).getId();
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            session.setAttribute("userId", userId);
            session.setAttribute("pageNumber", 0);
            Integer pageNumber = 0;
            List<UserRecord> userRecords = userRecordService.pagination(userId, pageNumber);
            Long userRecordsCount = userRecordService.userRecordsRowCount(userId);
            session.setAttribute("userRecordsCount", userRecordsCount);
            model.addObject("username", username);
            model.addObject("userRecords", userRecords);
            Long pageCount;
            if (userRecordsCount % 10 > 0) {
                pageCount = userRecordsCount / 10 + 1;
            } else {
                pageCount = userRecordsCount / 10;
            }
            model.addObject("pageCount", pageCount);
            session.setAttribute("pageCount", pageCount);
        }
        return model;
    }

    @RequestMapping(value = "logout.action")
    public ModelAndView logout(HttpSession session) {
        session.invalidate();
        return new ModelAndView("welcome");
    }
}