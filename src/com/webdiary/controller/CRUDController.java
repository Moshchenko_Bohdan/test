package com.webdiary.controller;

import com.webdiary.domain.UserRecord;
import com.webdiary.services.UserRecordService;
import com.webdiary.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
public class CRUDController {

    @Autowired
    UserService userService;

    @Autowired
    UserRecordService userRecordService;



    @RequestMapping(value = "createRecord.action")
    public ModelAndView createRecord() {
        ModelAndView model = new ModelAndView("createRecord");
        return model;
    }

    @RequestMapping(value = "editRecord.action")
    public ModelAndView editRecord(@RequestParam(value = "userRecordId", required = true) Long id) {
        ModelAndView model = new ModelAndView("editRecord");
        model.addObject("userRecord", userRecordService.getUserRecordByRecordId(id));
        return model;
    }

    @RequestMapping(value = "saveRecord.action")
    public ModelAndView saveRecord(HttpServletRequest request,
                                   @RequestParam(value = "record", required = true) String record) {
        Long userId = (Long) request.getSession().getAttribute("userId");
        UserRecord userRecord = new UserRecord(record, new Date(), userId);
        userRecordService.saveUserRecord(userRecord);
        Long userRecordsCount = userRecordService.userRecordsRowCount(userId);
        Long pageCount;
        if (userRecordsCount % 10 > 0) {
            pageCount = userRecordsCount / 10 + 1;
        } else {
            pageCount = userRecordsCount / 10;
        }
        ModelAndView model = new ModelAndView("userRecords");
        model.addObject("username", request.getSession().getAttribute("username"));
        model.addObject("userRecords", userRecordService.pagination(userId, pageCount.intValue() - 1));
        model.addObject("pageCount", pageCount);
        return model;
    }

    @RequestMapping(value = "saveChangedRecord.action")
    public ModelAndView saveRecordAfterEdit(HttpServletRequest request,
                                            @RequestParam(value = "id", required = true) Long id,
                                            @RequestParam(value = "record", required = true) String record) {
        Long userId = (Long) request.getSession().getAttribute("userId");
        UserRecord userRecord = userRecordService.getUserRecordByRecordId(id);
        userRecord.setRecord(record);
        userRecordService.saveUserRecord(userRecord);
        ModelAndView model = new ModelAndView("userRecords");
        model.addObject("username", request.getSession().getAttribute("username"));
        Long pageCount = (Long) request.getSession().getAttribute("pageCount");
        model.addObject("userRecords", userRecordService.pagination(userId, 0));
        model.addObject("pageCount", pageCount);
        return model;
    }

    @RequestMapping(value = "deleteRecord.action")
    public ModelAndView deleteRecord(HttpServletRequest request,
                                     @RequestParam(value = "userRecordId", required = true) Long id) {
        userRecordService.deleteUserRecordById(id);
        Long userId = (Long) request.getSession().getAttribute("userId");
        Long userRecordsCount = userRecordService.userRecordsRowCount(userId);
        Long pageCount;
        if (userRecordsCount % 10 > 0) {
            pageCount = userRecordsCount / 10 + 1;
        } else {
            pageCount = userRecordsCount / 10;
        }
        ModelAndView model = new ModelAndView("userRecords");
        model.addObject("username", request.getSession().getAttribute("username"));
        Integer pageNumber = (Integer) request.getSession().getAttribute("pageNumber");
        if (pageNumber <= pageCount) {
            model.addObject("userRecords", userRecordService.pagination(userId, pageNumber));
            request.getSession().setAttribute("pageNumber", pageNumber);
        } else {
            pageNumber -= 1;
            model.addObject("userRecords", userRecordService.pagination(userId, pageNumber));
            request.getSession().setAttribute("pageNumber", pageNumber);
        }
        model.addObject("pageCount", pageCount);
        return model;
    }

    @RequestMapping(value = "pagination.action")
    public ModelAndView pagination(HttpServletRequest request,
                                   @RequestParam(value = "pageNumber") Integer pageNumber) {
        ModelAndView model = new ModelAndView("userRecords");
        Long userId = (Long) request.getSession().getAttribute("userId");
        model.addObject("username", request.getSession().getAttribute("username"));
        model.addObject("userRecords", userRecordService.pagination(userId, pageNumber));
        request.getSession().setAttribute("pageNumber", pageNumber);
        return model;
    }
}