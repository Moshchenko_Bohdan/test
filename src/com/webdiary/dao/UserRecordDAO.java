package com.webdiary.dao;

import com.webdiary.domain.UserRecord;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRecordDAO {
    private final int maxResults = 10;

    @Autowired
    SessionFactory sessionFactory;

    public void saveUserRecord(UserRecord userRecord) {
        sessionFactory.getCurrentSession().saveOrUpdate(userRecord);
    }

    public UserRecord getUserRecordByRecordId(Long id) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserRecord.class);
        criteria.add(Restrictions.eq("id", id));
        return (UserRecord) criteria.uniqueResult();
    }

    public void deleteUserRecordById(Long id) {
        UserRecord userRecord = (UserRecord) sessionFactory.getCurrentSession().createCriteria(UserRecord.class)
                .add(Restrictions.eq("id", id)).uniqueResult();
        sessionFactory.getCurrentSession().delete(userRecord);
    }

    public List<UserRecord> pagination(Long userId, Integer pageNumber) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserRecord.class);
        criteria.add(Restrictions.eq("userId", userId));
        criteria.setFirstResult(pageNumber * maxResults);
        criteria.setMaxResults(maxResults);
        return criteria.list();
    }

    public Long userRecordsRowCount(Long userId) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserRecord.class);
        criteria.add(Restrictions.eq("userId", userId));
        return (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
    }
}