package com.webdiary.services;

import com.webdiary.dao.UserDAO;
import com.webdiary.dao.UserRecordDAO;
import com.webdiary.domain.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = false)
public class UserRecordService {

    @Autowired
    UserRecordDAO userRecordDAO;

    @Autowired
    UserDAO userDAO;

    public UserRecord getUserRecordByRecordId(Long id) {
        return userRecordDAO.getUserRecordByRecordId(id);
    }

    public boolean saveUserRecord(UserRecord userRecord) {
        boolean status;
        try {
            userRecordDAO.saveUserRecord(userRecord);
            status = true;
        } catch (Exception e) {
            status = false;
        }
        return status;
    }

    public void deleteUserRecordById(Long id) {
        userRecordDAO.deleteUserRecordById(id);
    }

    public List<UserRecord> pagination(Long userId, Integer pageNumber) {
        return userRecordDAO.pagination(userId, pageNumber);
    }

    public Long userRecordsRowCount(Long userId) {
        return userRecordDAO.userRecordsRowCount(userId);
    }
}