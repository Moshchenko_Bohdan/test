package com.webdiary.services;

import com.webdiary.dao.UserDAO;
import com.webdiary.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = false)
public class UserService {

    @Autowired
    UserDAO userDAO;

    public User getUserByUsername(String username) {
        return userDAO.getUserByUsername(username);
    }

    public boolean saveUser(User user) {
        boolean status;
        try {
            userDAO.saveUser(user);
            status = true;
        } catch (Exception e) {
            status = false;
        }
        return status;
    }

    public boolean userAuthentication(String username, String password) {
        boolean status = false;
        try {
            User user = userDAO.getUserByUsername(username);
            if ((user.getUsername().equals(username)) & (user.getPassword().equals(password))) {
                status = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return status;
    }
}