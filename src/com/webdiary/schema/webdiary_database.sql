-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.42 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4913
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных webdiary
CREATE DATABASE IF NOT EXISTS `webdiary` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `webdiary`;


-- Дамп структуры для таблица webdiary.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(50) NOT NULL,
  `password` char(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userName` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы webdiary.users: ~7 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`) VALUES
	(1, 'asd', '123'),
	(2, 'aaa', '777'),
	(4, 'sdfds', 'fdsfds'),
	(6, 'qwer', '1234'),
	(7, 'qaz', '123'),
	(8, 'sdf', '34'),
	(9, 'yura', '123456');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Дамп структуры для таблица webdiary.user_records
CREATE TABLE IF NOT EXISTS `user_records` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `record` longtext NOT NULL,
  `date` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_record_user` (`userId`),
  CONSTRAINT `FK_record_user` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы webdiary.user_records: ~46 rows (приблизительно)
DELETE FROM `user_records`;
/*!40000 ALTER TABLE `user_records` DISABLE KEYS */;
INSERT INTO `user_records` (`id`, `record`, `date`, `userId`) VALUES
	(33, '11111112233', '2015-04-03 09:31:45', 1),
	(48, 'xnxvnbxvnbxvbnewrwerewr', '2015-04-08 12:00:27', 1),
	(49, 'rewytrsyrtsy', '2015-04-08 12:00:31', 1),
	(50, 'cmnbnbcmnbmbcm 435435435', '2015-04-08 12:00:33', 1),
	(51, 'bvcnbcvncbvnnc', '2015-04-08 12:00:36', 1),
	(54, 'cvbnvcbnvcnvcnbvcnbcv', '2015-04-08 12:00:45', 1),
	(57, '6io myio;dej 33333', '2015-04-08 12:00:56', 1),
	(58, 'dsfh fhgzxfx ys', '2015-04-08 12:00:59', 1),
	(59, ' dbvncx jur', '2015-04-08 12:01:03', 1),
	(60, '6w5 uw:R rsohst', '2015-04-08 12:16:21', 1),
	(61, 'ewrwer', '2015-04-09 16:53:26', 1),
	(62, '324324', '2015-04-09 17:22:54', 1),
	(63, '67657', '2015-04-09 17:35:30', 2),
	(64, 'dfgdsfg', '2015-04-10 11:20:16', 1),
	(65, '!!!!!!', '2015-04-10 16:09:18', 1),
	(66, 'wedsffsd', '2015-04-10 16:26:27', 1),
	(67, '16:28', '2015-04-10 16:29:01', 1),
	(68, '16:31', '2015-04-10 16:31:53', 1),
	(69, '5646', '2015-04-10 16:32:07', 1),
	(70, '45645645', '2015-04-10 16:32:11', 1),
	(71, '64545645', '2015-04-10 16:32:14', 1),
	(72, '456234132', '2015-04-10 16:32:18', 1),
	(73, 'vcnbvcn', '2015-04-10 16:32:21', 1),
	(74, '12431 gfn dn tdjkmdjkdf,f d', '2015-04-10 16:32:33', 1),
	(75, '43154q 6y', '2015-04-10 16:32:36', 1),
	(76, '5y wtryy', '2015-04-10 16:32:40', 1),
	(78, 'dfbsdfb', '2015-04-10 17:43:47', 1),
	(79, 'bxvc nxn', '2015-04-10 17:43:52', 1),
	(80, 'xbvnbvnxvn', '2015-04-10 17:43:56', 1),
	(81, 'xnbvvbnxvnbxvn', '2015-04-10 17:44:00', 1),
	(82, 'wtrywtry hgfhxf', '2015-04-10 17:56:22', 1),
	(83, 'xnxvbnvn', '2015-04-24 10:12:45', 1),
	(85, '121212', '2015-04-24 12:45:00', 1),
	(86, '55555555555', '2015-04-24 12:48:49', 1),
	(87, 'h;.m/,mn//', '2015-04-27 13:08:09', 1),
	(89, '4wy5 by ', '2015-04-27 15:16:36', 1),
	(111, ';likhop', '2015-04-27 18:58:45', 1),
	(116, 'weqr wereqw', '2015-04-29 10:06:20', 1),
	(117, ' cgvdafgaqdr qtregar', '2015-04-29 10:06:26', 1),
	(118, ' qre tqtqrtq  qertq t', '2015-04-29 10:06:32', 1),
	(119, 'asdasd dfg we', '2015-04-29 11:27:58', 1),
	(121, 'dasfds fsdg dgg', '2015-04-29 11:41:34', 1),
	(122, 'wer vwer we', '2015-04-29 11:41:48', 1),
	(125, 'wewerewr  1           11', '2015-04-29 11:52:30', 1),
	(126, ' dsf', '2015-04-29 11:55:04', 1),
	(127, 'ert ertqt 11111111', '2015-04-29 12:27:09', 1);
/*!40000 ALTER TABLE `user_records` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
