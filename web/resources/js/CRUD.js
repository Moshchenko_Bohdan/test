$(document).ready(function () {
    $("#saveRecord").click(function () {
        var record = $("#record").val();
        $.post("/saveRecord.action", {record: record}, function (response) {
            $("#content").html(response);
            $("#content").show();
        })
    })

    $("#createRecord").click(function () {
        $.post("/createRecord.action", function (response) {
            $("#content").html(response);
            $("#content").show();
        })
    })

    $("div[name='editRecord']").click(function (event) {
        var userRecordId = $(event.target).attr("id");
        $.post("/editRecord.action", {userRecordId: userRecordId}, function (response) {
            $("#content").html(response);
            $("#content").show();
        })
    })

    $("div[name='deleteRecord']").click(function (event) {
        var userRecordId = $(event.target).attr("id");
        $.post("/deleteRecord.action", {userRecordId: userRecordId}, function (response) {
            $("#content").html(response);
            $("#content").show();
        })
    });

    $("div[name='pagination']").click(function (event) {
        var pageNumber = $(event.target).attr("id");
        $.post("/pagination.action", {pageNumber: pageNumber - 1}, function (response) {
            $("#content").html(response);
            $("#content").show();
        })
    });
});