$(document).ready(function () {
    $("#register").click(function () {
        var username = $("#username").val();
        var password = $("#password").val();
        $.post("/register.action", {username: username, password: password}, function (response) {
            $("#loginForm").hide();
            $("#content").html(response);
            $("#content").show();
        })
    })

    $("#login").click(function () {
        var username = $("#username").val();
        var password = $("#password").val();
        $.post("/loginUser.action", {username: username, password: password}, function (response) {
            $("#loginForm").hide();
            $("#content").html(response);
            $("#content").show();
        })
    })

    $("#logout").click(function () {
        $.post("/logout.action", function (response) {
            document.write(response);
        })
    })
})