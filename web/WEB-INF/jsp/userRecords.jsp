<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="/resources/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/resources/js/Login.js"></script>
<script type="text/javascript" src="/resources/js/CRUD.js"></script>
<link rel="stylesheet" type="text/css" href="/resources/css/main.css">

<script>
    var count = ${pageCount};
    var pageNumbers = $("#pageNumbers");
    for (var i = 1; i <= count; i++) {
        pageNumbers.html(pageNumbers.html() + " " + "<div name=" + "'pagination'" + "class=" + "'" + "divLink" + "'" + "style=" + "'" + "display: inline-table" + "'" + "id=" + "'" + i + "'" + ">" + i + "</div>");
    }
</script>

<h3>Hello, ${username}</h3>

<div id="logout" name="logout" class="divLink">Logout</div>
<div id="createRecord" name="createRecord" class="divLink">createRecord</div>

<table border="1">
    <c:forEach var="userRecord" items="${userRecords}">
        <tr>
            <td><c:out value="${userRecord.record}"/></td>
            <td><c:out value="${userRecord.date}"/></td>
            <td>
                <div name="editRecord" id="${userRecord.id}" class="divLink" style="display: inline-table">Edit</div>
                <div name="deleteRecord" id="${userRecord.id}" class="divLink" style="display: inline-table">Delete
                </div>
            </td>
        </tr>
    </c:forEach>
</table>

<div id="pageNumbers"></div>