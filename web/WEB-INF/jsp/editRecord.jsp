<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit record</title>
</head>
<body>

<script>
    $(document).ready(function () {
        $("#saveChanges").click(function () {
            var record = $("#record").val();
            $.post("/saveChangedRecord.action", {id: ${userRecord.id}, record: record}, function (response) {
                $("#content").html(response);
                $("#content").show();
            })
        })
    })
</script>

<div id="editRecord">
    <textarea id="record" rows="10" cols="100" name="record">${userRecord.record}</textarea>
    <input id="saveChanges" type="button" value="Save changes">
</div>

</body>
</html>
