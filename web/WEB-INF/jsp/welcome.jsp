<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="/resources/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/resources/js/Login.js"></script>
    <script type="text/javascript" src="/resources/js/CRUD.js"></script>
    <link rel="stylesheet" type="text/css" href="/resources/css/main.css">
</head>
<body>

<div id="loginForm">
    <input id="username" type="text" name="username">
    <input id="password" type="password" name="password">

    <div style="display: block">
        <input id="register" type="button" value="Register">
        <input id="login" type="button" value="Login">
    </div>
</div>

<div id="content" class="hiddenComponent"></div>

</body>
</html>