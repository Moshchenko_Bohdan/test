<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="/resources/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/resources/js/Login.js"></script>
</head>
<body>

<script>
    $(document).ready(function () {
        $("#saveRecord").click(function () {
            var record = $("#record").val();
            $.post("/saveRecord.action", {record: record}, function (response) {
                $("#content").html(response);
                $("#content").show();
            })
        })
    })
</script>

<div id="createRecord">
    <textarea id="record" rows="10" cols="100" name="record"></textarea>
    <input id="saveRecord" type="button" value="Save Record">
</div>

</body>
</html>